package gpxt.archive;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by mario on 16/03/2016.
 */
public class OAuthCredentials implements Serializable {
    private static final Logger logger = Logger.getLogger(OAuthCredentials.class.getName());
    private static final long serialVersionUID = 1L;
    /**
     * Directory to store user credentials.
     */
    private static final String keyStore = System.getProperty("user.home") + "/.gpxshare-strava";

    private static String code;

    public static String getCode() {
        return code;
    }

    public static void setCode(String token) {
        OAuthCredentials.code = token;
    }
    public void store() {
        try {
            OutputStream os = new FileOutputStream(keyStore);
            OutputStream buffer = new BufferedOutputStream(os);
            ObjectOutput output = new ObjectOutputStream(buffer);
            try {
                output.writeObject(this);
            } finally {
                output.close();
            }
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Cannot store credentials", ex);
        }
    }

    /**
     * Check file exists java.io.FileNotFoundException: /home/user/.oauthstore-strava (No such file or directory)
     *
     * @return
     */
    public static OAuthCredentials read() {
        OAuthCredentials credentials = null;

        try (InputStream file = new FileInputStream(keyStore);
             InputStream buffer = new BufferedInputStream(file);
             ObjectInput input = new ObjectInputStream(buffer);) {
            // deserialize the List

            try {
                credentials = (OAuthCredentials) input.readObject();
            } finally {
                input.close();
            }
        } catch (ClassNotFoundException ex) {
            logger.log(Level.SEVERE, "Cannot perform input. Class not found.",
                    ex);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Cannot perform input.", ex);
        }

        return credentials;
    }
}
