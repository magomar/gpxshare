package gpxt;

/**
 * OAuth 2 credentials from your service provider. You will need to create an
 * account and enter your clientId and clientSecret here (don't share these
 * globally).
 */
public class StravaConnection {


    public final static String authServer = "https://www.strava.com/gpxt.oauth/authorize";
    public final static String tokenServer = "https://www.strava.com/gpxt.oauth/token";

    /**
     * Port in the "Callback URL".
     */
    public final static int port = 8081;
    /**
     * Domain name in the "Callback URL".
     */
    public final static String domain = "localhost";

    // CHANGE THESE TO YOUR VALUES
    public final static String clientId = "5560";
    public final static String clientSecret = "231651146aa53123ab97d2cfd964519c6940cca4 ";
}