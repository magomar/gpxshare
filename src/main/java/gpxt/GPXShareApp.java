package gpxt;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by mario on 16/03/2016.
 */
public class GPXShareApp extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/views/MainView.fxml"));

        Scene scene = new Scene(root, 640, 480);

        stage.setScene(scene);
        stage.setTitle("GPX Share");
        stage.setMinWidth(320);
        stage.setMinHeight(300);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
