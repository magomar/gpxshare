package gpxt.controller;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javastrava.api.v3.model.StravaActivity;
import javastrava.api.v3.model.reference.StravaActivityType;

import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

public class ActivitiesViewController implements Initializable {

    @FXML
    private AnchorPane mainPane;

    @FXML
    private TableView<StravaActivity> activities;

    @FXML
    private TableColumn<StravaActivity, StravaActivityType> type;

    @FXML
    private TableColumn<StravaActivity, String> title;

    @FXML
    private TableColumn<StravaActivity, LocalDate> date;

    @FXML
    private TableColumn<StravaActivity, Integer> time;

    @FXML
    private TableColumn<StravaActivity, Float> distance;

    @FXML
    private TableColumn<StravaActivity, Float> height;

    private ObservableList<StravaActivity> data = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        activities.setItems(data);
        type.setCellValueFactory(new PropertyValueFactory<>("type"));
        title.setCellValueFactory(new PropertyValueFactory<>("name"));
        date.setCellValueFactory(new PropertyValueFactory<>("startDateLocal"));
        time.setCellValueFactory(new PropertyValueFactory<>("movingTime"));
        distance.setCellValueFactory(new PropertyValueFactory<>("distance"));
        height.setCellValueFactory(new PropertyValueFactory<>("totalElevationGain"));
    }

    public void setActivities(List<StravaActivity> activities) {
        data.clear();
        data.addAll(activities);
    }
}
