package gpxt.controller;

import gpxt.oauth.Authentification;
import gpxt.oauth.OAuth2Credentials;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javastrava.api.v3.auth.AuthorisationService;
import javastrava.api.v3.auth.impl.retrofit.AuthorisationServiceImpl;
import javastrava.api.v3.auth.model.Token;
import javastrava.api.v3.auth.model.TokenResponse;
import javastrava.api.v3.auth.ref.AuthorisationScope;
import javastrava.api.v3.model.StravaActivity;
import javastrava.api.v3.model.StravaAthlete;
import javastrava.api.v3.rest.API;
import javastrava.api.v3.rest.AuthorisationAPI;
import javastrava.api.v3.rest.async.StravaAPIFuture;
import javastrava.api.v3.service.Strava;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author mario
 */
public class MainViewController implements Initializable {
    @FXML
    private AnchorPane mainPane;

    @FXML
    private Button listActivitiesButton;

    private OAuth2Credentials credentials;
    private Strava strava = null;
    private API api;
    private Token token = null;
    private Integer athleteId = 7117519;
    private AuthorisationScope[] scopes = {AuthorisationScope.VIEW_PRIVATE, AuthorisationScope.WRITE};
    private BooleanProperty connected = new SimpleBooleanProperty(false);
    @FXML
    private ActivitiesViewController activitiesPaneController;

    private void authenticate() {
        String code = Authentification.getOAuth2Credentials();
        System.out.println("New code: " + code);
        credentials = new OAuth2Credentials();
        credentials.setClientToken(code);
        credentials.Store();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        listActivitiesButton.disableProperty().bind(connected.not());
        credentials = OAuth2Credentials.Read();
        if (null == credentials) {
            authenticate();
        } else {
            System.out.println("Retrieved code: " + credentials.getClientToken());
        }
    }

    @FXML
    private void fullConnection(ActionEvent event) {
        if (null != credentials.getClientToken()) {
            AuthorisationService service = new AuthorisationServiceImpl();
            token = service.tokenExchange(Integer.valueOf(OAuth2Credentials.clientId),
                    OAuth2Credentials.clientSecret,
                    credentials.getClientToken(), scopes);
        }
        if (null != token) {
            strava = new Strava(token);
            connected.set(true);
            String account;
            account = token.getAthlete().getFirstname() + " " + token.getAthlete().getLastname();
//            StravaAthlete athlete = strava.getAthlete(athleteId);
            System.out.println("Connected to Strava as : " + account);
        }

    }

    @FXML
    private void rawSyncConnection(ActionEvent event) {
        if (null != credentials.getClientToken()) {
            AuthorisationAPI auth = API.authorisationInstance();
            TokenResponse response = auth.tokenExchange(Integer.valueOf(OAuth2Credentials.clientId),
                    OAuth2Credentials.clientSecret,
                    credentials.getClientToken());
            token = new Token(response, scopes); //scopes are optional
        }
        if (null != token) {
            api = new API(token);
            StravaAthlete athlete = api.getAthlete(athleteId);
            System.out.println("Athlete: " + athlete.getFirstname());
        }
    }

    @FXML
    private void rawAsyncConnection(ActionEvent event) {
        if (null != credentials.getClientToken()) {
            AuthorisationAPI auth = API.authorisationInstance();
            TokenResponse response = auth.tokenExchange(Integer.valueOf(OAuth2Credentials.clientId), OAuth2Credentials.clientSecret, credentials.getClientToken());
            token = new Token(response, scopes);
        }
        if (null != token)
            api = new API(token);
        StravaAPIFuture<StravaAthlete> future = api.getAthleteAsync(athleteId);

// Now you can do something else while you wait for the result
        // doSomethingInterestingInsteadOfWaiting();

// And when you're ready, get the athlete from the future...
        StravaAthlete athlete = future.get();
        System.out.println("Athlete: " + athlete.getFirstname());
    }


    @FXML
    private void listActivities(ActionEvent event) {
        List<StravaActivity> activities = null;
        if (null != strava) {
            activities = strava.listAllAuthenticatedAthleteActivities();
        } else if (null != api) {
//            activities = api.listAuthenticatedAthleteActivities(LocalTime.now(), LocalDate.of(0,0,0),10,20);
        }
        System.out.println(activities);

        activitiesPaneController.setActivities(activities);
    }

    private void exit(ActionEvent event) {
        mainPane.getScene().getWindow().hide();
    }

/*
        OAuth2Credentials credentials = OAuth2Credentials.Read();
        if (credentials != null) {
            // see if we can use them to talk to server
            String code = credentials.getClientToken();
            System.out.println("===========================");
            System.out.println("Retrieved code: " + code);
            if (null != code) {
                token = getToken(code, scopes);
                System.out.println("bearer token.1 " + token.getToken());
            }
        }

        if (credentials == null || token == null) {
            String code = Authentification.getOAuth2Credentials();
            System.out.println("===========================");
            System.out.println("New code: " + code);
            credentials = new OAuth2Credentials();
            credentials.setClientToken(code);
            credentials.Store();
            token = getToken(code, scopes);
            System.out.println("bearer token.2 " + token.getToken());
        }
        strava = new Strava(token);
        System.out.println("Athlete: " + token.getAthlete().getFirstname() + " " + token.getAthlete().getLastname());


*/
}
